import XCTest
@testable import CoinUp

class TickerTests: XCTestCase {

    func testThatTickerIsInitiated() {
        // Given
        let doubleArray: SocketTicker = [1.0, 3.0, 1.0, 3.0, 1.0, 3.0, 1.0, 3.0, 1.0, 3.0, 4.0]
        // When
        let ticker = Ticker(doubleArray)
        // Then
        XCTAssertNotNil(ticker)
    }

    func testThatTickerIsNotInitiated() {
        // Given
        let doubleArray: SocketTicker = [1.0, 3.0]
        // When
        let ticker = Ticker(doubleArray)
        // Then
        XCTAssertNil(ticker)
    }
}

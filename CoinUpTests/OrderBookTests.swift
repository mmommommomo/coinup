import XCTest
@testable import CoinUp

class OrderBookTests: XCTestCase {

    func testThatOrderBookIsInitiated() {
        // Given
        let doubleArray: SocketOrderBook = [1.0, 3.0, 1.0, 3.0, 1.0, 3.0, 1.0, 3.0, 1.0, 3.0, 4.0]
        // When
        let order = OrderBook(doubleArray)
        // Then
        XCTAssertNotNil(order)
    }

    func testThatOrderBookIsNotInitiated() {
        // Given
        let doubleArray: SocketOrderBook = [1.0, 3.0]
        // When
        let order = OrderBook(doubleArray)
        // Then
        XCTAssertNil(order)
    }
}

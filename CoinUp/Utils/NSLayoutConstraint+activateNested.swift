import UIKit

public extension NSLayoutConstraint {
    static func activateNested(_ constraints: [[NSLayoutConstraint]]) {
        constraints.joined().forEach { constraint in
            (constraint.firstItem as? UIView)?.translatesAutoresizingMaskIntoConstraints = false
        }
        NSLayoutConstraint.activate(constraints.flatMap { $0 })
    }

    static func deactivateNested(_ constraints: [[NSLayoutConstraint]]) {
        NSLayoutConstraint.deactivate(constraints.flatMap { $0 })
    }
}

import Foundation
import UIKit

extension UIColor {
    static let flatGreen = #colorLiteral(red: 0.1851693392, green: 0.7989721298, blue: 0.443267405, alpha: 1)
    static let flatRed = #colorLiteral(red: 0.905113399, green: 0.2976434231, blue: 0.2338240445, alpha: 1)
    static let flatOrange = #colorLiteral(red: 0.8534849882, green: 0.4655888677, blue: 0.1318592131, alpha: 1)
    static let flatBlack = #colorLiteral(red: 0.1686089635, green: 0.1686392426, blue: 0.1686022878, alpha: 1)
    static let flatGray = #colorLiteral(red: 0.4958557487, green: 0.5506778359, blue: 0.5540549159, alpha: 1)
}

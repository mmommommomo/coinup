import UIKit

public extension UIView {
    typealias SignedMargin = CGFloat

    @discardableResult
    func constraint(replacing old: [NSLayoutConstraint], with new: [NSLayoutConstraint]) -> [NSLayoutConstraint] {
        NSLayoutConstraint.deactivate(old)
        removeConstraints(old)
        return new
    }

    func constraint<T>(replacingAnchor anchor: NSLayoutAnchor<T>, with new: [NSLayoutConstraint]) -> [NSLayoutConstraint] {
        let old = constraints.filter { $0.firstAnchor == anchor }
        return constraint(replacing: old, with: new)
    }

    func constraintEdges(to view: UIView? = nil, useSafeArea: Bool = false, margins: SignedMargin = 0, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        let horizontalConstraints = constraintEdgesHorizontally(to: view, useSafeArea: useSafeArea, margins: margins, relatedBy: relation, priority: priority)
        let verticalConstraints =  constraintEdgesVertically(to: view, useSafeArea: useSafeArea, margins: margins, relatedBy: relation, priority: priority)
        return horizontalConstraints + verticalConstraints
    }

    func constraintEdgesHorizontally(to view: UIView? = nil, useSafeArea: Bool = false, margins: SignedMargin = 0, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        let leading = constraintLeading(to: view, useSafeArea: useSafeArea, margin: margins, relatedBy: relation, priority: priority)
        let trailing = constraintTrailing(to: view, useSafeArea: useSafeArea, margin: margins, relatedBy: relation, priority: priority)
        return leading + trailing
    }

    func constraintEdgesVertically(to view: UIView? = nil, useSafeArea: Bool = false, margins: SignedMargin = 0, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        let top = constraintTop(to: view, useSafeArea: useSafeArea, margin: margins, relatedBy: relation, priority: priority)
        let bottom = constraintBottom(to: view, useSafeArea: useSafeArea, margin: margins, relatedBy: relation, priority: priority)
        return top + bottom
    }

    func constraintLeading(toTrailing view: UIView, margin: SignedMargin = 0, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        return constraintLeading(toAnchor: view.trailingAnchor, margin: margin, relatedBy: relation, priority: priority)
    }

    func constraintTop(toBottom view: UIView, margin: SignedMargin = 0, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        return constraintTop(toAnchor: view.bottomAnchor, margin: margin, relatedBy: relation, priority: priority)
    }

    func constraintLeading(to view: UIView? = nil, useSafeArea: Bool = false, margin: SignedMargin = 0, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        let constraintsView = view ?? superview!
        let leadingAnchor: NSLayoutXAxisAnchor
        if useSafeArea {
            leadingAnchor = constraintsView.safeAreaLayoutGuide.leadingAnchor
        } else {
            leadingAnchor = constraintsView.leadingAnchor
        }
        return constraintLeading(toAnchor: leadingAnchor, margin: margin, relatedBy: relation, priority: priority)
    }

    func constraintLeading(toAnchor: NSLayoutXAxisAnchor, margin: SignedMargin = 0, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        return constraintXAxisAnchor(leadingAnchor, toAnchor: toAnchor, constant: margin, relatedBy: relation, priority: priority)
    }

    func constraintTrailing(to view: UIView? = nil, useSafeArea: Bool = false, margin: SignedMargin = 0, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        let constraintsView = view ?? superview!
        let trailingAnchor: NSLayoutXAxisAnchor
        if useSafeArea {
            trailingAnchor = constraintsView.safeAreaLayoutGuide.trailingAnchor
        } else {
            trailingAnchor = constraintsView.trailingAnchor
        }
        return constraintTrailing(toAnchor: trailingAnchor, margin: margin, relatedBy: relation, priority: priority)
    }

    func constraintTrailing(toAnchor: NSLayoutXAxisAnchor, margin: SignedMargin = 0, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        return constraintXAxisAnchor(trailingAnchor, toAnchor: toAnchor, constant: -margin, relatedBy: relation, priority: priority)
    }

    func constraintTop(to view: UIView? = nil, useSafeArea: Bool = false, margin: SignedMargin = 0, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        let constraintsView = view ?? superview!
        let topAnchor: NSLayoutYAxisAnchor
        if useSafeArea {
            topAnchor = constraintsView.safeAreaLayoutGuide.topAnchor
        } else {
            topAnchor = constraintsView.topAnchor
        }
        return constraintTop(toAnchor: topAnchor, margin: margin, relatedBy: relation, priority: priority)
    }

    func constraintTop(toAnchor: NSLayoutYAxisAnchor, margin: SignedMargin = 0, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        return constraintYAxisAnchor(topAnchor, toAnchor: toAnchor, constant: margin, relatedBy: relation, priority: priority)
    }

    func constraintBottom(to view: UIView? = nil, useSafeArea: Bool = false, margin: SignedMargin = 0, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        let constraintsView = view ?? superview!
        let bottomAnchor: NSLayoutYAxisAnchor
        if useSafeArea {
            bottomAnchor = constraintsView.safeAreaLayoutGuide.bottomAnchor
        } else {
            bottomAnchor = constraintsView.bottomAnchor
        }
        return constraintBottom(toAnchor: bottomAnchor, margin: margin, relatedBy: relation, priority: priority)
    }

    func constraintBottom(toAnchor: NSLayoutYAxisAnchor, margin: SignedMargin = 0, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        return constraintYAxisAnchor(bottomAnchor, toAnchor: toAnchor, constant: -margin, relatedBy: relation, priority: priority)
    }

    func constraintCenter(to view: UIView? = nil, multiplier: CGPoint = CGPoint(x: 1, y: 1), displacement: (x: SignedMargin, y: SignedMargin) = (0, 0), relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        let x = constraintCenterX(to: view, multiplier: multiplier.x, displacement: displacement.x, relatedBy: relation, priority: priority)
        let y = constraintCenterY(to: view, multiplier: multiplier.y, displacement: displacement.y, relatedBy: relation, priority: priority)
        return x + y
    }

    func constraintCenterX(to view: UIView? = nil, multiplier: CGFloat = 1, displacement: SignedMargin = 0, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        let constraintsView = view ?? superview!
        return constraintAttribute(.centerX,
                                   relatedBy: relation,
                                   to: constraintsView,
                                   attribute: .centerX,
                                   multiplier: multiplier,
                                   constant: displacement,
                                   priority: priority)
    }

    func constraintCenterY(to view: UIView? = nil, multiplier: CGFloat = 1, displacement: SignedMargin = 0, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        let constraintsView = view ?? superview!
        return constraintAttribute(.centerY,
                                   relatedBy: relation,
                                   to: constraintsView,
                                   attribute: .centerY,
                                   multiplier: multiplier,
                                   constant: displacement,
                                   priority: priority)
    }

    func constraintSize(width: CGFloat, height: CGFloat, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        return constraintSize(CGSize(width: width, height: height),
                              relatedBy: relation,
                              priority: priority)
    }

    func constraintSize(_ size: CGSize, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        let width: [NSLayoutConstraint]
        let height: [NSLayoutConstraint]
        width = constraintWidth(size.width, relatedBy: relation, priority: priority)
        height = constraintHeight(size.height, relatedBy: relation, priority: priority)
        return width + height
    }

    func constraintSize(to view: UIView? = nil, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        let width: [NSLayoutConstraint]
        let height: [NSLayoutConstraint]
        if let view = view {
            width = constraintWidth(toAnchor: view.widthAnchor, relatedBy: relation, priority: priority)
            height = constraintHeight(toAnchor: view.heightAnchor, relatedBy: relation, priority: priority)
        } else {
            width = constraintWidth(relatedBy: relation, priority: priority)
            height = constraintHeight(relatedBy: relation, priority: priority)
        }
        return width + height
    }

    func constraintWidth(_ width: CGFloat, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        return constraintDimensionAnchor(widthAnchor, constant: width, relatedBy: relation, priority: priority)
    }

    func constraintWidth(to view: UIView, multiplier: CGFloat = 1, constant: CGFloat = 0, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        let view = view
        return constraintWidth(toAnchor: view.widthAnchor, multiplier: multiplier, constant: constant, relatedBy: relation, priority: priority)
    }

    func constraintWidth(toAnchor: NSLayoutDimension? = nil, multiplier: CGFloat = 1, constant: CGFloat = 0, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        let anchor = toAnchor ?? superview!.widthAnchor
        return constraintDimensionAnchor(widthAnchor, toAnchor: anchor, multiplier: multiplier, constant: constant, relatedBy: relation, priority: priority)
    }

    func constraintHeight(_ height: CGFloat, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        return constraintDimensionAnchor(heightAnchor, constant: height, relatedBy: relation, priority: priority)
    }

    func constraintHeight(to view: UIView, multiplier: CGFloat = 1, constant: CGFloat = 0, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        return constraintHeight(toAnchor: view.heightAnchor, multiplier: multiplier, constant: constant, relatedBy: relation, priority: priority)
    }

    func constraintHeight(toAnchor: NSLayoutDimension? = nil, multiplier: CGFloat = 1, constant: CGFloat = 0, relatedBy relation: NSLayoutConstraint.Relation = .equal, priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        let anchor = toAnchor ?? superview!.heightAnchor
        return constraintDimensionAnchor(heightAnchor, toAnchor: anchor, multiplier: multiplier, constant: constant, relatedBy: relation, priority: priority)
    }

    func constraintAttribute(_ receiverAttribute: NSLayoutConstraint.Attribute,
                             relatedBy relation: NSLayoutConstraint.Relation = .equal,
                             to view: UIView,
                             attribute toAttribute: NSLayoutConstraint.Attribute,
                             multiplier: CGFloat = 1,
                             constant: SignedMargin = 0,
                             priority: UILayoutPriority = .required) -> [NSLayoutConstraint] {
        let constraint = NSLayoutConstraint(item: self,
                                            attribute: receiverAttribute,
                                            relatedBy: relation,
                                            toItem: view,
                                            attribute: toAttribute,
                                            multiplier: multiplier,
                                            constant: constant)
        constraint.priority = priority
        return [constraint]
    }

    func distributeHorizontallyViewsInSuperview(_ views: [UIView]) {
        guard let firstView = views.first else { return }
        guard let lastView = views.last else { return }

        var constraints: [[NSLayoutConstraint]] = [
            firstView.constraintLeading(),
            lastView.constraintTrailing()
        ]
        for i in 1..<views.count {
            let view = views[i]
            let previousView = views[i-1]
            constraints.append(view.constraintLeading(toTrailing: previousView))
        }
        NSLayoutConstraint.activateNested(constraints)
    }

    // MARK: Private

    private func constraintYAxisAnchor(_ anchor: NSLayoutXAxisAnchor, constant: SignedMargin, relatedBy relation: NSLayoutConstraint.Relation, priority: UILayoutPriority) -> [NSLayoutConstraint] {
        return constraintXAxisAnchor(anchor, toAnchor: anchor, constant: constant, relatedBy: relation, priority: priority)
    }

    private func constraintXAxisAnchor(_ anchor: NSLayoutXAxisAnchor, toAnchor: NSLayoutXAxisAnchor, constant: SignedMargin, relatedBy relation: NSLayoutConstraint.Relation, priority: UILayoutPriority) -> [NSLayoutConstraint] {
        let constraint: NSLayoutConstraint
        switch relation {
        case .equal:
            constraint = anchor.constraint(equalTo: toAnchor, constant: constant)
        case .greaterThanOrEqual:
            constraint = anchor.constraint(greaterThanOrEqualTo: toAnchor, constant: constant)
        case .lessThanOrEqual:
            constraint = anchor.constraint(lessThanOrEqualTo: toAnchor, constant: constant)
        @unknown default:
            fatalError()
        }
        constraint.priority = priority
        return [constraint]
    }

    private func constraintYAxisAnchor(_ anchor: NSLayoutYAxisAnchor, constant: SignedMargin, relatedBy relation: NSLayoutConstraint.Relation, priority: UILayoutPriority) -> [NSLayoutConstraint] {
        return constraintYAxisAnchor(anchor, toAnchor: anchor, constant: constant, relatedBy: relation, priority: priority)
    }

    private func constraintYAxisAnchor(_ anchor: NSLayoutYAxisAnchor, toAnchor: NSLayoutYAxisAnchor, constant: SignedMargin, relatedBy relation: NSLayoutConstraint.Relation, priority: UILayoutPriority) -> [NSLayoutConstraint] {
        let constraint: NSLayoutConstraint
        switch relation {
        case .equal:
            constraint = anchor.constraint(equalTo: toAnchor, constant: constant)
        case .greaterThanOrEqual:
            constraint = anchor.constraint(greaterThanOrEqualTo: toAnchor, constant: constant)
        case .lessThanOrEqual:
            constraint = anchor.constraint(lessThanOrEqualTo: toAnchor, constant: constant)
        @unknown default:
            fatalError()

        }
        constraint.priority = priority
        return [constraint]
    }

    private func constraintDimensionAnchor(_ anchor: NSLayoutDimension, constant: CGFloat, relatedBy relation: NSLayoutConstraint.Relation, priority: UILayoutPriority) -> [NSLayoutConstraint] {
        let constraint: NSLayoutConstraint
        switch relation {
        case .equal:
            constraint = anchor.constraint(equalToConstant: constant)
        case .greaterThanOrEqual:
            constraint = anchor.constraint(greaterThanOrEqualToConstant: constant)
        case .lessThanOrEqual:
            constraint = anchor.constraint(lessThanOrEqualToConstant: constant)
        @unknown default:
            fatalError()
        }
        constraint.priority = priority
        return [constraint]
    }

    private func constraintDimensionAnchor(_ anchor: NSLayoutDimension, toAnchor: NSLayoutDimension, multiplier: CGFloat, constant: CGFloat, relatedBy relation: NSLayoutConstraint.Relation, priority: UILayoutPriority) -> [NSLayoutConstraint] {
        let constraint: NSLayoutConstraint
        switch relation {
        case .equal:
            constraint = anchor.constraint(equalTo: toAnchor, multiplier: multiplier, constant: constant)
        case .greaterThanOrEqual:
            constraint = anchor.constraint(greaterThanOrEqualTo: toAnchor, multiplier: multiplier, constant: constant)
        case .lessThanOrEqual:
            constraint = anchor.constraint(lessThanOrEqualTo: toAnchor, multiplier: multiplier, constant: constant)
        @unknown default:
            fatalError()
        }
        constraint.priority = priority
        return [constraint]
    }
}

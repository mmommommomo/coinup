import Foundation
import UIKit

class TitleValueView: UIView {
    private let hasTitle: Bool
    private let title: String?
    private let horizontalMargin: CGFloat = 10

    public init(_ title: String? = nil) {
        self.hasTitle = title != nil
        self.title = title?.uppercased()
        super.init(frame: CGRect.zero)
        setupView()
        setupConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12, weight: .light)
        label.textColor = UIColor.flatGray
        return label
    }()

    private let valueLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        label.textColor = UIColor.flatBlack
        label.text = "..."
        return label
    }()

    func setupView() {
        backgroundColor = .white
        addSubview(titleLabel)
        addSubview(valueLabel)
        titleLabel.text = title
    }

    func setupConstraints() {
        NSLayoutConstraint.activateNested([
            titleLabel.constraintEdgesHorizontally(margins: horizontalMargin),
            titleLabel.constraintTop(),
            titleLabel.constraintHeight(20),
            valueLabel.constraintEdgesVertically(),
            valueLabel.constraintEdgesHorizontally(margins: horizontalMargin)
        ])
    }

    func updateValue(_ value: String) {
        valueLabel.text = value
    }

    func updateValue(_ value: String, color: UIColor) {
        updateValue(value)
        valueLabel.textColor = color
    }
}

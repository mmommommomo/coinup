import UIKit

protocol Coordinator: class {
    func start()
    var viewController: UIViewController? { get }
    var parent: Coordinator? { get }
}

class AppCoordinator: Coordinator {
    var parent: Coordinator?
    let navigationController: UINavigationController
    var viewController: UIViewController? {
        return navigationController
    }
	var window: UIWindow
	let overviewCoordinator: OverviewCoordinator

	init(window: UIWindow) {
		self.window = window
        let btcUSD = SymbolPair(identifier: "tBTCUSD",
                                title: "BTC/USD",
                                localeIdentifier: "en_US")
        overviewCoordinator = OverviewCoordinator(symbolPair: btcUSD)
        self.navigationController = UINavigationController()
        window.rootViewController = navigationController
	}

    func start() {
        overviewCoordinator.start()
        guard let viewController = overviewCoordinator.viewController else {
            assertionFailure("No overview view controller set.")
            return
        }
        self.navigationController.viewControllers = [viewController]
    }
}

import UIKit

class OrderBookViewController: UIViewController, OrderBookViewModelViewDelegate {

    let viewModel: OrderBookViewModelProtocol
    let buyTableViewController: OrderBookTableViewController
    let sellTableViewController: OrderBookTableViewController

    init(viewModel: OrderBookViewModelProtocol) {
        self.viewModel = viewModel
        self.buyTableViewController = OrderBookTableViewController(title: "Buy Orders", textColor: .flatGreen)
        self.sellTableViewController = OrderBookTableViewController(title: "Sell Orders", textColor: .flatRed)
        super.init(nibName: nil, bundle: nil)
    }

    override func loadView() {
        let orderBookView = OrderBookView()
        self.view = orderBookView
        self.title = viewModel.symbolPair.title
        addChild(buyTableViewController)
        addChild(sellTableViewController)

        orderBookView.addChildTable(buyTableViewController.view, in: orderBookView.buyContainer)
        orderBookView.addChildTable(sellTableViewController.view, in: orderBookView.sellContainer)

        buyTableViewController.tableView.dataSource = viewModel.buyDataSource
        sellTableViewController.tableView.dataSource = viewModel.sellDataSource
    }

    override func viewWillAppear(_ animated: Bool) {
        viewModel.viewWillAppear()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func showOrderBookEntry(_ entry: OrderBookViewEntry, _ entryType: OrderType, _ shouldReloadTableView: Bool) {
        let total: Int
        let tableView: UITableView
        switch entryType {
        case .ask:
            tableView = buyTableViewController.tableView
            total = viewModel.buyDataSource.totalEntries
        case .bid:
            tableView = sellTableViewController.tableView
            total = viewModel.sellDataSource.totalEntries
        }
        let indexPath = IndexPath(item: total - 1, section: 0)
        if shouldReloadTableView {
            tableView.reloadData() // TODO: Handle the insertRows instead of reloadData
        } else {
            tableView.insertRows(at: [indexPath], with: .automatic)
        }
        tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
    }
}

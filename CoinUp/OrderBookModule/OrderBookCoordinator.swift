import Foundation
import UIKit

protocol OrderBookCoordinatorDelegate: class {
    func didStart(_ orderBookViewController: OrderBookViewController)
}

class OrderBookCoordinator: Coordinator {
    var viewController: UIViewController?
    weak var parent: Coordinator?
    private let symbolPair: SymbolPair
    private var orderBookModel: OrderBookModel?

    init(symbolPair: SymbolPair) {
        self.symbolPair = symbolPair
	}

    weak var delegate: OrderBookCoordinatorDelegate?

    func start() {
        let model = OrderBookModel(repository: OrderBookDataRepository(symbolPair: symbolPair))
        let viewModel = OrderBookViewModel(symbolPair: symbolPair, model: model)
        orderBookModel = model

        let viewController = OrderBookViewController(viewModel: viewModel)
        viewModel.viewDelegate = viewController
        viewModel.coordinatorDelegate = self
        self.viewController = viewController
        delegate?.didStart(viewController)
    }

    func subscribeToOrderBook() {
        orderBookModel?.subscribeToOrderBook()
    }
}

extension OrderBookCoordinator: OrderBookViewModelCoordinatorDelegate {

    func viewModelDidReceive(connectionEvent: ConnectionEvent) {
        if let overviewCoordinator = parent as? OverviewCoordinator {
            overviewCoordinator.handleConnexionEvent(connectionEvent)
        }
    }
}

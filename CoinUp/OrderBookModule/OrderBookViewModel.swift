import Foundation

protocol OrderBookViewModelCoordinatorDelegate: class {
    func viewModelDidReceive(connectionEvent: ConnectionEvent)
}

protocol OrderBookViewModelViewDelegate: class {
    func showOrderBookEntry(_ entry: OrderBookViewEntry, _ entryType: OrderType, _ shouldReloadTableView: Bool)
}

protocol OrderBookViewModelProtocol {
    var symbolPair: SymbolPair { get }
    var model: OrderBookModelProtocol { get }
    var viewDelegate: OrderBookViewModelViewDelegate? { get set }
    var coordinatorDelegate: OrderBookViewModelCoordinatorDelegate? { get set }
    var buyDataSource: OrderBookTableViewDataSource { get }
    var sellDataSource: OrderBookTableViewDataSource { get }
    func viewWillAppear()
}

class OrderBookViewModel: OrderBookViewModelProtocol {
    let symbolPair: SymbolPair
    private(set) var model: OrderBookModelProtocol
    weak var viewDelegate: OrderBookViewModelViewDelegate?
    weak var coordinatorDelegate: OrderBookViewModelCoordinatorDelegate?
    let buyDataSource: OrderBookTableViewDataSource
    let sellDataSource: OrderBookTableViewDataSource

    init(symbolPair: SymbolPair, model: OrderBookModelProtocol) {
        self.symbolPair = symbolPair
        self.model = model
        self.buyDataSource = OrderBookTableViewDataSource()
        self.sellDataSource = OrderBookTableViewDataSource()
        self.model.delegate = self
	}

    func viewWillAppear() {
        model.subscribeToOrderBook()
    }

    func prepareOrderBook(_ orderBook: OrderBook) {
        let entry = OrderBookViewEntry(orderBook)
        let shouldReloadTableView: Bool
        switch orderBook.type {
        case .ask:
            shouldReloadTableView = buyDataSource.add(entry)
        case .bid:
            shouldReloadTableView = sellDataSource.add(entry)
        }
        viewDelegate?.showOrderBookEntry(entry, orderBook.type, shouldReloadTableView)
    }
}

extension OrderBookViewModel: OrderBookModelDelegate {
    func connectionEvent(_ event: ConnectionEvent) {
        coordinatorDelegate?.viewModelDidReceive(connectionEvent: event)
    }

    func didReceiveOrderBook(_ OrderBook: OrderBook) {
        prepareOrderBook(OrderBook)
    }
}

import UIKit

class OrderBookView: UIView {
    private let horizontalMargin: CGFloat = 10

    public init() {
        super.init(frame: CGRect.zero)
        setupView()
        setupConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    let buyContainer = UIView()
    let sellContainer = UIView()

    func setupView() {
        addSubview(buyContainer)
        addSubview(sellContainer)
    }

    func setupConstraints() {
        NSLayoutConstraint.activateNested([
            buyContainer.constraintEdgesVertically(),
            buyContainer.constraintLeading(margin: horizontalMargin),
            sellContainer.constraintEdgesVertically(),
            sellContainer.constraintLeading(toTrailing: buyContainer, margin: horizontalMargin),
            sellContainer.constraintTrailing(margin: horizontalMargin),
            sellContainer.constraintWidth(toAnchor: buyContainer.widthAnchor)
        ])
    }

    // MARK: Update data in view

    func addChildTable(_ tableView: UIView, in container: UIView) {
        container.addSubview(tableView)
        NSLayoutConstraint.activateNested([
            tableView.constraintEdges()
        ])
    }
}

import Foundation
import UIKit

class OrderBookTableViewCell: UITableViewCell {

    var priceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .light)
        label.textColor = UIColor.flatGray
        return label
    }()

    var amountLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .light)
        label.textColor = UIColor.flatGray
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupConstraints()
        separatorInset = .zero
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func setupView() {
        addSubview(priceLabel)
        addSubview(amountLabel)
    }

    func setupConstraints() {
        NSLayoutConstraint.activateNested([
            priceLabel.constraintEdgesVertically(),
            priceLabel.constraintLeading(),
            amountLabel.constraintEdgesVertically(),
            amountLabel.constraintLeading(toTrailing: priceLabel),
            amountLabel.constraintTrailing(),
            amountLabel.constraintWidth(toAnchor: priceLabel.widthAnchor)
            ])
    }

    func fill(with entry: OrderBookViewEntry) {
        priceLabel.text = entry.price
        amountLabel.text = entry.amount
    }
}

class OrderBookTableViewHeader: UIView {
    let titleLabel: UILabel
    let leftLabel: UILabel
    let rightLabel: UILabel

    public init(title: String, leftTitle: String, rightTitle: String, textColor: UIColor) {
        func makeLabel(_ title: String, _ textColor: UIColor) -> UILabel {
            let label = UILabel()
            label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
            label.textColor = textColor
            label.text = title
            return label
        }
        titleLabel = makeLabel(title, textColor)
        leftLabel = makeLabel(leftTitle, .flatBlack)
        rightLabel = makeLabel(rightTitle, .flatBlack)
        super.init(frame: .zero)
        setupView()
        setupConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupView() {
        backgroundColor = .white
        addSubview(titleLabel)
        addSubview(leftLabel)
        addSubview(rightLabel)
    }

    func setupConstraints() {
        NSLayoutConstraint.activateNested([
            titleLabel.constraintEdgesHorizontally(),
            titleLabel.constraintTop(),
            titleLabel.constraintHeight(40),
            leftLabel.constraintTop(toBottom: titleLabel),
            leftLabel.constraintBottom(),
            leftLabel.constraintLeading(),
            rightLabel.constraintTop(toBottom: titleLabel),
            rightLabel.constraintBottom(),
            rightLabel.constraintLeading(toTrailing: leftLabel),
            rightLabel.constraintTrailing(),
            rightLabel.constraintWidth(toAnchor: leftLabel.widthAnchor)
        ])
    }
}

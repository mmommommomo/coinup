import Foundation
import UIKit

class OrderBookTableViewController: UITableViewController {
    static let cellReuseIdentifier: String = "OrderBookTableViewCell"
    let textColor: UIColor

    init(title: String, textColor: UIColor) {
        self.textColor = textColor
        super.init(style: .plain)
        self.title = title
        self.tableView.delegate = self
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        tableView.register(OrderBookTableViewCell.self, forCellReuseIdentifier: OrderBookTableViewController.cellReuseIdentifier)
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return OrderBookTableViewHeader(title: self.title ?? "", leftTitle: "PRICE", rightTitle: "AMOUNT", textColor: textColor)
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }

    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 1))
    }
}

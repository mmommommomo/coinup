import Foundation
import UIKit

class OrderBookTableViewDataSource: NSObject, UITableViewDataSource {
    private var entries: [OrderBookViewEntry]
    private let entriesCountLimit: Int
    private let visibleEntries: Int

    init(entriesCountLimit: Int = 50, visibleEntries: Int = 20) {
        entries = []
        self.entriesCountLimit = entriesCountLimit
        self.visibleEntries = visibleEntries
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OrderBookTableViewController.cellReuseIdentifier, for: indexPath) as! OrderBookTableViewCell
        cell.fill(with: entries[indexPath.row])
        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return entries.count
    }

    /// Adds an entry in the list.
    /// If the total of items exceeds the limit, the list is cleaned
    ///
    /// - parameter entry: The entry to add to the list
    ///
    /// - returns: A boolean saying if the list has been cleaned
    func add(_ entry: OrderBookViewEntry) -> Bool {
        var wasCleaned = false
        let totalEntries = self.entries.count
        if totalEntries > entriesCountLimit {
            let lastIndex = totalEntries - 1
            let startIndex = lastIndex - visibleEntries
            self.entries = Array(entries[startIndex..<lastIndex])
            wasCleaned = true
        }
        self.entries.append(entry)
        return wasCleaned
    }

    var totalEntries: Int {
        return entries.count
    }
}

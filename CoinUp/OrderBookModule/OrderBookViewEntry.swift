import Foundation

struct OrderBookViewEntry {
    let price: String
    let amount: String

    init(_ orderBook: OrderBook) {
        price = "\(orderBook.price)"
        amount = "\(orderBook.amount)"
    }
}

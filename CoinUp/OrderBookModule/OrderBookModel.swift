import Foundation
import RxSwift

protocol OrderBookModelProtocol {
    func subscribeToOrderBook()
    var delegate: OrderBookModelDelegate? { get set }
}

protocol OrderBookModelDelegate: class {
    func didReceiveOrderBook(_ OrderBook: OrderBook)
    func connectionEvent(_ event: ConnectionEvent)
}

class OrderBookModel: OrderBookModelProtocol {
    private let OrderBookRepository: OrderBookRepository
    private let disposeBag = DisposeBag()
    weak var delegate: OrderBookModelDelegate?

    init(repository: OrderBookRepository) {
        self.OrderBookRepository = repository
    }

    func subscribeToOrderBook() {
        OrderBookRepository.connect()
        OrderBookRepository
            .OrderBookData
            .subscribe(onNext: { [weak self] OrderBook in
                guard let self = self else { return }
                self.delegate?.didReceiveOrderBook(OrderBook)
            })
            .disposed(by: disposeBag)

        OrderBookRepository
            .connectionEvent
            .subscribe(onNext: { [weak self] event in
                guard let self = self else { return }
                self.delegate?.connectionEvent(event)
            })
            .disposed(by: disposeBag)
    }
}

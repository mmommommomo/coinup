import Foundation

typealias SocketTicker = [Double]

extension SocketTicker {
    static func decodeTicker(_ text: String) throws -> SocketTicker? {
        if let data = text.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let doubleArray = json as? [Double] {
                    return doubleArray
                }
            } catch {
                throw error
            }
        }
        return nil
    }
}

import Foundation

typealias SocketOrderBook = [Double]

extension SocketOrderBook {
    static func decodeOrder(_ text: String) throws -> SocketOrderBook? {
        if let data = text.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let doubleArray = json as? [Double] {
                    return doubleArray
                }
            } catch {
                throw error
            }
        }
        return nil
    }
}

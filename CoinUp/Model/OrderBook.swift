import Foundation

enum OrderType {
    case bid
    case ask
}

struct OrderBook {
    let channel: Int
    let type: OrderType
    let price: Double
    let count: Double
    let amount: Double

    init?(_ socket: SocketOrderBook) {
        guard socket.count == 4 else {
            return nil
        }
        channel = Int(socket[0])
        price = socket[1]
        count = socket[2]
        let amountvalue = socket[3]
        type = amountvalue > 0 ? .bid : .ask
        amount = amountvalue < 0 ? (amountvalue * -1) :  amountvalue
    }
}

import Foundation

struct SymbolPair {
    let identifier: String
    let title: String
    let localeIdentifier: String
}

import Foundation

struct Ticker {
    let channel: Int
    let bid: Double
    let bidSize: Double
    let ask: Double
    let askSize: Double
    let dailyChange: Double
    let dailyChangePercent: Double
    let lastPrice: Double
    let volume: Double
    let high: Double
    let low: Double

    init?(_ socketTicker: SocketTicker) {
        guard socketTicker.count == 11 else {
            return nil
        }
        channel = Int(socketTicker[0])
        bid = socketTicker[1]
        bidSize = socketTicker[2]
        ask = socketTicker[3]
        askSize = socketTicker[4]
        dailyChange = socketTicker[5]
        dailyChangePercent = socketTicker[6]
        lastPrice = socketTicker[7]
        volume = socketTicker[8]
        high = socketTicker[9]
        low = socketTicker[10]
    }
}

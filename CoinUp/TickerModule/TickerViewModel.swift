import Foundation

protocol TickerViewModelCoordinatorDelegate: class {
    func viewModelDidReceive(connectionEvent: ConnectionEvent)
}

protocol TickerViewModelViewDelegate: class {
    func showTickerInfo(_ info: TickerViewInfo)
}

protocol TickerViewModelProtocol {
    var symbolPair: SymbolPair { get }
    var model: TickerModelProtocol { get }
    var viewDelegate: TickerViewModelViewDelegate? { get set }
    var coordinatorDelegate: TickerViewModelCoordinatorDelegate? { get set}
    func viewWillAppear()
}

class TickerViewModel: TickerViewModelProtocol {
    let symbolPair: SymbolPair
    private(set) var model: TickerModelProtocol
    weak var viewDelegate: TickerViewModelViewDelegate?
    weak var coordinatorDelegate: TickerViewModelCoordinatorDelegate?

    init(symbolPair: SymbolPair, model: TickerModelProtocol) {
        self.symbolPair = symbolPair
        self.model = model
        self.model.delegate = self
	}

    func viewWillAppear() {
        model.subscribeToTicker()
    }

    func prepareTicker(_ ticker: Ticker) {
        let info = TickerViewInfo(ticker, locale: symbolPair.localeIdentifier)
        viewDelegate?.showTickerInfo(info)
    }
}

extension TickerViewModel: TickerModelDelegate {
    func connectionEvent(_ event: ConnectionEvent) {
        coordinatorDelegate?.viewModelDidReceive(connectionEvent: event)
    }

    func didReceiveTicker(_ ticker: Ticker) {
        prepareTicker(ticker)
    }
}

import UIKit

class TickerView: UIView {
    public init() {
        super.init(frame: CGRect.zero)
        setupView()
        setupConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private let symbolPairView = TitleValueView()
    private let lastView = TitleValueView("Last")
    private let volumeView = TitleValueView("Volume")
    private let percentageView = TitleValueView()
    private let highView = TitleValueView("High")
    private let lowView = TitleValueView("Low")

    private let verticalStack: UIStackView = {
        let stack = UIStackView()
        stack.distribution = .fillEqually
        stack.axis = .vertical
        return stack
    }()

    func setupView() {
        self.backgroundColor = .yellow
        addSubview(verticalStack)
        verticalStack.addArrangedSubview(horizontalStack(symbolPairView, lastView))
        verticalStack.addArrangedSubview(horizontalStack(volumeView, percentageView))
        verticalStack.addArrangedSubview(horizontalStack(highView, lowView))
    }

    private func horizontalStack(_ left: TitleValueView, _ right: TitleValueView) -> UIStackView {
        let stack = UIStackView(arrangedSubviews: [left, right])
        stack.distribution = .fillEqually
        stack.axis = .horizontal
        return stack
    }

    func setupConstraints() {
        NSLayoutConstraint.activateNested([
            verticalStack.constraintEdges()
        ])
    }

    // MARK: Update data in view

    func setSymbolPairTitle(title: String) {
        symbolPairView.updateValue(title)
    }

    func update(with info: TickerViewInfo) {
        lastView.updateValue(info.last)
        volumeView.updateValue(info.volume)
        percentageView.updateValue(info.percentage, color: info.percentageColor)
        lowView.updateValue(info.low)
        highView.updateValue(info.high)
    }
}

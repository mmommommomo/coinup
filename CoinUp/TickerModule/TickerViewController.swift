import UIKit

class TickerViewController: UIViewController, TickerViewModelViewDelegate {
    let viewModel: TickerViewModelProtocol

    init(viewModel: TickerViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    override func loadView() {
        let tickerView = TickerView()
        tickerView.setSymbolPairTitle(title: viewModel.symbolPair.title)
        self.view = tickerView
        self.title = viewModel.symbolPair.title
    }

    override func viewWillAppear(_ animated: Bool) {
        viewModel.viewWillAppear()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func showTickerInfo(_ info: TickerViewInfo) {
        guard let tickerView = self.view as? TickerView else {
            return
        }
        tickerView.update(with: info)
    }
}

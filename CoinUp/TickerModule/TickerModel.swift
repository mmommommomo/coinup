import Foundation
import RxSwift

protocol TickerModelProtocol {
    func subscribeToTicker()
    var delegate: TickerModelDelegate? { get set }
}

protocol TickerModelDelegate: class {
    func didReceiveTicker(_ ticker: Ticker)
    func connectionEvent(_ event: ConnectionEvent)
}

class TickerModel: TickerModelProtocol {
    private let tickerRepository: TickerRepository
    private let disposeBag = DisposeBag()
    weak var delegate: TickerModelDelegate?

    init(repository: TickerRepository) {
        self.tickerRepository = repository
    }

    func subscribeToTicker() {
        tickerRepository.connect()
        tickerRepository
            .tickerData
            .subscribe(onNext: { [weak self] ticker in
                guard let self = self else { return }
                self.delegate?.didReceiveTicker(ticker)
            })
            .disposed(by: disposeBag)

        tickerRepository
            .connectionEvent
            .subscribe(onNext: { [weak self] event in
                guard let self = self else { return }
                self.delegate?.connectionEvent(event)
            })
            .disposed(by: disposeBag)
    }
}

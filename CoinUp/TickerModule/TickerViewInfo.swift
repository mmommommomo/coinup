import UIKit

struct TickerViewInfo {
    let last: String
    let volume: String
    let percentage: String
    let percentageColor: UIColor
    let low: String
    let high: String

    init(_ ticker: Ticker, locale: String) {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: locale)

        last = formatter.string(from: NSNumber(value: ticker.lastPrice)) ?? ""
        volume = "\(ticker.volume)"
        percentage = "\(ticker.dailyChangePercent) %"
        percentageColor = ticker.dailyChangePercent > 0 ? .flatGreen : .flatRed
        low = formatter.string(from: NSNumber(value: ticker.low)) ?? ""
        high = formatter.string(from: NSNumber(value: ticker.high)) ?? ""
    }
}

import Foundation
import UIKit

protocol TickerCoordinatorDelegate: class {
    func didStart(_ tickerViewController: TickerViewController)
}

class TickerCoordinator: Coordinator {
    var viewController: UIViewController?
    weak var parent: Coordinator?
    private let symbolPair: SymbolPair
    private var tickerModel: TickerModel?

    init(symbolPair: SymbolPair) {
        self.symbolPair = symbolPair
	}

    weak var delegate: TickerCoordinatorDelegate?

    func start() {
        let model = TickerModel(repository: TickerDataRepository(symbolPair: symbolPair))
        let viewModel = TickerViewModel(symbolPair: symbolPair, model: model)
        tickerModel = model

        let viewController = TickerViewController(viewModel: viewModel)
        viewModel.viewDelegate = viewController
        viewModel.coordinatorDelegate = self
        self.viewController = viewController
        delegate?.didStart(viewController)
    }

    func subscribeToTicker() {
        tickerModel?.subscribeToTicker()
    }
}

extension TickerCoordinator: TickerViewModelCoordinatorDelegate {

    func viewModelDidReceive(connectionEvent: ConnectionEvent) {
        if let overviewCoordinator = parent as? OverviewCoordinator {
            overviewCoordinator.handleConnexionEvent(connectionEvent)
        }
    }
}

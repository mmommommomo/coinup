import Foundation
import Starscream
import RxSwift

protocol OrderBookRepository {
    func connect()
    var OrderBookData: Observable<OrderBook> { get }
    var connectionEvent: Observable<ConnectionEvent> { get }
}

class OrderBookDataRepository: OrderBookRepository {
    private let dataSource: OrderBookDataSource

    ///Default Init with default data sources
    init(symbolPair: SymbolPair) {
        let url = "wss://api-pub.bitfinex.com/ws/"
        let socketClient = WebSocket(url: URL(string: url)!)
        self.dataSource = OrderBookSocketDataSource(socketClient: socketClient, symbolPair: symbolPair)
    }

    func connect() {
        dataSource.connect()
    }

    var OrderBookData: Observable<OrderBook> { return dataSource.OrderBookData }
    var connectionEvent: Observable<ConnectionEvent> { return dataSource.connectionEvent }
}

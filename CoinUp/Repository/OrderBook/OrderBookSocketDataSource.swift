import Foundation
import Starscream
import RxSwift

protocol OrderBookDataSource {
    func connect()
    var OrderBookData: Observable<OrderBook> { get }
    var connectionEvent: Observable<ConnectionEvent> { get }
}

class OrderBookSocketDataSource: OrderBookDataSource {
    private let socketClient: WebSocketClient
    private let symbolPair: SymbolPair
    fileprivate let OrderBookSubject = PublishSubject<OrderBook>()
    fileprivate let connectionSubject = PublishSubject<ConnectionEvent>()

    init(socketClient: WebSocketClient, symbolPair: SymbolPair) {
        self.socketClient = socketClient
        self.symbolPair = symbolPair
    }

    func connect() {
        socketClient.delegate = self
        socketClient.connect()
    }

    var OrderBookData: Observable<OrderBook> {
        return OrderBookSubject
    }

    var connectionEvent: Observable<ConnectionEvent> {
        return connectionSubject
    }

    private func sendSocketSubscribe(_ socket: WebSocketClient) {
        let json: [String: String] = [
            "event": "subscribe",
            "channel": "book",
            "symbol": symbolPair.identifier
        ]
        if let jsonData = try? JSONSerialization.data(withJSONObject: json, options: []) {
            socket.write(data: jsonData)
        }
    }
}

extension OrderBookSocketDataSource: WebSocketDelegate {
    func websocketDidConnect(socket: WebSocketClient) {
        connectionSubject.onNext(.connected)
        sendSocketSubscribe(socket)
    }

    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        connectionSubject.onNext(.disconnected)
    }

    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        do {
            if let socketOrderBook = try SocketOrderBook.decodeOrder(text),
                let OrderBook = OrderBook(socketOrderBook) {
                OrderBookSubject.onNext(OrderBook)
            }
        } catch {
            connectionSubject.onNext(.didReceiveError(error: error))
        }
    }

    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        // Not handled, this socked only receives String
    }
}

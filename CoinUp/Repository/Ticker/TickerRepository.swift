import Foundation
import Starscream
import RxSwift

protocol TickerRepository {
    func connect()
    var tickerData: Observable<Ticker> { get }
    var connectionEvent: Observable<ConnectionEvent> { get }
}

class TickerDataRepository: TickerRepository {
    private let dataSource: TickerDataSource

    ///Default Init with default data sources
    init(symbolPair: SymbolPair) {
        let url = "wss://api-pub.bitfinex.com/ws/"
        let socketClient = WebSocket(url: URL(string: url)!)
        self.dataSource = TickerSocketDataSource(socketClient: socketClient, symbolPair: symbolPair)
    }

    func connect() {
        dataSource.connect()
    }

    var tickerData: Observable<Ticker> { return dataSource.tickerData }
    var connectionEvent: Observable<ConnectionEvent> { return dataSource.connectionEvent }
}

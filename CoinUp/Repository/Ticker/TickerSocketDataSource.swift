import Foundation
import Starscream
import RxSwift

protocol TickerDataSource {
    func connect()
    var tickerData: Observable<Ticker> { get }
    var connectionEvent: Observable<ConnectionEvent> { get }
}

enum ConnectionEvent {
    case connected
    case disconnected
    case didReceiveError(error: Error)
}

class TickerSocketDataSource: TickerDataSource {
    private let socketClient: WebSocketClient
    private let symbolPair: SymbolPair
    fileprivate let tickerSubject = PublishSubject<Ticker>()
    fileprivate let connectionSubject = PublishSubject<ConnectionEvent>()

    init(socketClient: WebSocketClient, symbolPair: SymbolPair) {
        self.socketClient = socketClient
        self.symbolPair = symbolPair
    }

    func connect() {
        socketClient.delegate = self
        socketClient.connect()
    }

    var tickerData: Observable<Ticker> {
        return tickerSubject
    }

    var connectionEvent: Observable<ConnectionEvent> {
        return connectionSubject
    }

    private func sendSocketSubscribe(_ socket: WebSocketClient) {
        let json: [String: String] = [
            "event": "subscribe",
            "channel": "ticker",
            "symbol": symbolPair.identifier
        ]
        if let jsonData = try? JSONSerialization.data(withJSONObject: json, options: []) {
            socket.write(data: jsonData)
        }
    }
}

extension TickerSocketDataSource: WebSocketDelegate {
    func websocketDidConnect(socket: WebSocketClient) {
        connectionSubject.onNext(.connected)
        sendSocketSubscribe(socket)
    }

    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        connectionSubject.onNext(.disconnected)
    }

    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        do {
            if let socketTicker = try SocketTicker.decodeTicker(text),
                let ticker = Ticker(socketTicker) {
                tickerSubject.onNext(ticker)
            }
        } catch {
            connectionSubject.onNext(.didReceiveError(error: error))
        }
    }

    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        // Not handled, this socked only receives String
    }
}

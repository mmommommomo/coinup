import UIKit

class OverviewViewController: UIViewController, OverviewViewModelViewDelegate {
    var viewModel: OverviewViewModelProtocol
    private let overviewView = OverviewView()

    init(viewModel: OverviewViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        self.view = overviewView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = viewModel.symbolPair.title
        addLiveButton()
    }

    func addLiveButton() {
        overviewView.liveButton.addTarget(self, action: #selector(tappedLiveButton), for: .touchUpInside)
        self.navigationItem.setRightBarButtonItems([UIBarButtonItem(customView: overviewView.liveButton)],
                                                   animated: false)
    }

    @objc func tappedLiveButton() {
        viewModel.handleLiveButtonTap()
    }

    func addChildTicker(_ controller: TickerViewController) {
        self.addChild(controller)
        overviewView.tickerContainer.addSubview(controller.view)
        NSLayoutConstraint.activateNested([
            controller.view.constraintEdges()
        ])
    }

    func addChildOrderBook(_ controller: OrderBookViewController) {
        self.addChild(controller)
        overviewView.orderBookContainer.addSubview(controller.view)
        NSLayoutConstraint.activateNested([
            controller.view.constraintEdges()
        ])
    }

    func didUpdateConnectionButtonStatus(_ status: ConnectionButtonStatus) {
        overviewView.updateButtonStatus(status)
    }
}

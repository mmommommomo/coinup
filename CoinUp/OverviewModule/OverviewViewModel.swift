import Foundation
import UIKit

protocol OverviewViewModelCoordinatorDelegate: class {
    func sendConnectionRequest()
}

protocol OverviewViewModelViewDelegate: class {
    func didUpdateConnectionButtonStatus(_ status: ConnectionButtonStatus)
}

protocol OverviewViewModelProtocol {
    var symbolPair: SymbolPair { get }
    var viewDelegate: OverviewViewModelViewDelegate? { get set }
    var coordinatorDelegate: OverviewViewModelCoordinatorDelegate? { get set}
    func handleLiveButtonTap()
}

struct ConnectionButtonStatus {
    let title: String
    let color: UIColor

    init(event: ConnectionEvent) {
        switch event {
        case .connected:
            title = "LIVE"
            color = UIColor.flatGreen
        case .disconnected:
            title = "OFFLINE"
            color = UIColor.flatOrange
        case .didReceiveError:
            title = "ERROR (Try again)"
            color = UIColor.flatRed
        }
    }
}

class OverviewViewModel: OverviewViewModelProtocol {
    let symbolPair: SymbolPair
    weak var viewDelegate: OverviewViewModelViewDelegate?
    var coordinatorDelegate: OverviewViewModelCoordinatorDelegate?
    private var lastConnectionEvent: ConnectionEvent?

    init(symbolPair: SymbolPair) {
        self.symbolPair = symbolPair
	}

    func handleLiveButtonTap() {
        guard let lastEvent = lastConnectionEvent else { return}
        switch lastEvent {
        case .didReceiveError, .disconnected:
            coordinatorDelegate?.sendConnectionRequest()
        case .connected:
            print("Nothing should happen, we're connected.")
        }
    }

    func handleConnectionEvent(_ event: ConnectionEvent) {
        lastConnectionEvent = event
        let status = ConnectionButtonStatus(event: event)
        viewDelegate?.didUpdateConnectionButtonStatus(status)
    }
}

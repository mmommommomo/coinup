import UIKit

class OverviewView: UIView {
    public init() {
        super.init(frame: CGRect.zero)
        setupView()
        setupConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    let liveButton: UIButton = {
        let button = UIButton(type: .custom)
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 0.5
        return button
    }()

    let tickerContainer = UIView()
    let orderBookContainer = UIView()

    // MARK: - View setup

    func setupView() {
        backgroundColor = .white
        addSubview(tickerContainer)
        addSubview(orderBookContainer)
    }

    func setupConstraints() {
        NSLayoutConstraint.activateNested([
            liveButton.constraintWidth(80),
            liveButton.constraintHeight(30),
            tickerContainer.constraintEdgesHorizontally(useSafeArea: true),
            tickerContainer.constraintTop(useSafeArea: true),
            tickerContainer.constraintHeight(200),
            orderBookContainer.constraintEdgesHorizontally(useSafeArea: true),
            orderBookContainer.constraintTop(toBottom: tickerContainer),
            orderBookContainer.constraintBottom(useSafeArea: true)
        ])
    }

    func updateButtonStatus(_ status: ConnectionButtonStatus) {
        liveButton.setTitle(status.title, for: .normal)
        liveButton.setTitleColor(status.color, for: .normal)
        liveButton.layer.borderColor = status.color.cgColor
    }
}

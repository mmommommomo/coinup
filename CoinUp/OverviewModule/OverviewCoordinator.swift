import UIKit

protocol OverviewCoordinatorDelegate: class {}

class OverviewCoordinator: Coordinator {
    var viewController: UIViewController?
    weak var parent: Coordinator?
    private let symbolPair: SymbolPair
    private let tickerCoordinator: TickerCoordinator
    private let orderBookCoordinator: OrderBookCoordinator
    private var viewModel: OverviewViewModel?

    init(symbolPair: SymbolPair) {
        self.symbolPair = symbolPair
        tickerCoordinator = TickerCoordinator(symbolPair: symbolPair)
        orderBookCoordinator = OrderBookCoordinator(symbolPair: symbolPair)
	}

    weak var delegate: OverviewCoordinatorDelegate?

    func start() {
        let overviewViewModel =  OverviewViewModel(symbolPair: symbolPair)
        let viewController = OverviewViewController(viewModel: overviewViewModel)
        overviewViewModel.viewDelegate = viewController
        overviewViewModel.coordinatorDelegate = self
        self.viewModel = overviewViewModel
        self.viewController = viewController

        tickerCoordinator.parent = self
        tickerCoordinator.delegate = self
        tickerCoordinator.start()

        orderBookCoordinator.parent = self
        orderBookCoordinator.delegate = self
        orderBookCoordinator.start()
    }

    func handleConnexionEvent(_ event: ConnectionEvent) {
        guard let overviewViewModel = viewModel else { return }
        overviewViewModel.handleConnectionEvent(event)
    }
}

extension OverviewCoordinator: OverviewViewModelCoordinatorDelegate {
    func sendConnectionRequest() {
        tickerCoordinator.subscribeToTicker()
        orderBookCoordinator.subscribeToOrderBook()
    }
}

extension OverviewCoordinator: TickerCoordinatorDelegate {

    func didStart(_ tickerViewController: TickerViewController) {
        guard let viewController = viewController as? OverviewViewController else { return }
        viewController.addChildTicker(tickerViewController)
    }
}

extension OverviewCoordinator: OrderBookCoordinatorDelegate {
    func didStart(_ orderBookViewController: OrderBookViewController) {
        guard let viewController = viewController as? OverviewViewController else { return }
        viewController.addChildOrderBook(orderBookViewController)
    }
}

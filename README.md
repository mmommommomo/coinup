# CoinUp : Technical Test

Hi there! Thanks for taking the time to review my exercise.
Here are some information about the project! Enjoy 😀

## 📖 About the project

I've used MVVM+C as architecture, using coordinators.
I would need more time to implement some testing for those coordinators.
For the networking part, I decided to use StarScream and RxSwift. StarScream seemed an easy "plug and play" solution to handle the socket.
The Rx-Swift usage is very low in this app. I decided to not overload the views with RxSwift logic, as I don't think it's needed.

I also added a "status" button in the navigation bar. It allows to reconnect to the socket if the connection is lost, and get to know the connection status.

## 📲 Running the project

Open the terminal, then run the following command

```ruby
pod install
```
After that, open **CoinUp.xcworkspace** with XCode.

## 🛠 Technical details / Tools / External libs used
- Swift 5.0
- SwiftLint (Checks the code syntax)
- StarScream (Socket connection)
- RxSwift

## 👨🏼‍💻 Known issues
- There is a UI glitch when the list of OrderBooks entry exceeds the memory limit. I fixed the memory issue, but it introduced this UI glitch. Unfortunately I don't have time now to fix it.

## 👨🏼‍💻 TODO
- Test the coordinators and view model
- ...

## 👍🏼 Credits
- App Icon : [https://www.instagram.com/p/Bqz_rBnhYcu/](https://www.instagram.com/p/Bqz_rBnhYcu/)
